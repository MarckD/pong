﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ball_arcanoid : MonoBehaviour
{
    private Vector2 ballPosition;
    public Vector2 ballVelocity;
    public GameObject pala;
    public Text vidaslabel;
    public int vidasvalue = 3;

    void Awake()
    {
        ballPosition = transform.position;
        vidaslabel.text = vidasvalue.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        ballPosition.y = transform.position.y + ballVelocity.y * Time.deltaTime;
        ballPosition.x = transform.position.x + ballVelocity.x * Time.deltaTime;

        transform.position = new Vector3(ballPosition.x, ballPosition.y, transform.position.z);
    }

    /// <summary>
    /// Sent when an incoming collider makes contact with this object's
    /// collider (2D physics only).
    /// </summary>
    /// <param name="other">The Collision2D data associated with this collision.</param>
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Muro")
        {
            ballVelocity.y *= -1;
        }
        if (other.gameObject.tag == "MuroLateral")
        {
            ballVelocity.x *= -1;
        }
        else if (other.gameObject.tag == "EnemyGoal")
        {
            transform.position = new Vector3( -3.99f, -3.2f, 0);
            ballVelocity.y *= -1;

            pala.transform.position = new Vector3(-4.03f, pala.transform.position.y, pala.transform.position.z);

            vidasvalue --;

            vidaslabel.text = vidasvalue.ToString();

   
        }
        else if (other.gameObject.tag == "bloques")
        {
            ballVelocity.y *= -1;

            other.gameObject.transform.position = new Vector3(1000, other.gameObject.transform.position.y, other.gameObject.transform.position.z);

        }
        else if (other.gameObject.tag == "Pala")
        {
            ballVelocity.y *= -1;
        }
    }


}
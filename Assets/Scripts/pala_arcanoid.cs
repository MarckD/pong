﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pala_arcanoid : MonoBehaviour
{
    public float velocidadX;
    private float posX;
    private float direction;
    private Vector3 newPosition = new Vector3(0, 0, 0);

    // Update is called once per frame
    void Update()

    {

        direction = Input.GetAxis("Horizontal");

        Debug.Log(transform.position.x);
        Debug.Log(transform.rotation.x);

        posX = transform.position.x + direction * velocidadX * Time.deltaTime;

        if (posX > 0.34f)
        {
            posX = 0.34f;
        }
        if (posX < -8.2f)
        {
            posX = -8.2f;
        } 

        newPosition.y = transform.position.y;
        newPosition.x = posX;
        newPosition.z = transform.position.z;
        transform.position = newPosition;


    }
}